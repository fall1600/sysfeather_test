<?php

namespace App\Listeners;

use App\Mail\PaymentSuccessEmail;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendPaymentSuccessEmailListener
{
    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //todo implement email to user
        $payment = $event->getPayment();
        $this->mailer
            ->to($payment->email)
            ->send(new PaymentSuccessEmail());
//        $this->mailer->queue("mails.payments.success.email", new PaymentSuccessEmail());
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\SendPaymentSuccessEmailEvent',
            'App\Listeners\SendPaymentSuccessEmailListener@handle'
        );
    }
}
