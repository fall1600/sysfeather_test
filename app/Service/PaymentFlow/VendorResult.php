<?php

namespace App\Service\PaymentFlow;

class VendorResult
{
    protected $result;

    protected $payment;

    protected $isApiSuccess = false;

    public function __construct($payment, $result)
    {
        if ($result instanceof \Exception) {
            $this->isApiSuccess = false;
        }
        $this->isApiSuccess = true;
        $this->result = $result;
        $this->payment = $payment;
    }

    public function getTransactionId(): string
    {
        return $this->result['transaction_id']??"";
    }

    public function getIsPaid(): bool
    {
        return $this->result['is_paid'] ?? false;
    }

    public function getAmount()
    {
        return $this->result['amount']?? null;
    }

    public function isAPISuccess(): bool
    {
        return $this->isApiSuccess;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }
}
