<?php

namespace App\Events;

use App\Service\PaymentFlow\VendorResult;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendPaymentSuccessEmailEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $result;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VendorResult $result)
    {
        $this->result = $result;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return VendorResult
     */
    public function getResult(): VendorResult
    {
        return $this->result;
    }

    public function getPayment()
    {
        return $this->result ? $this->result->getPayment() : null;
    }
}
