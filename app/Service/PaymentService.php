<?php

namespace App\Service;

use App\Events\SendPaymentSuccessEmailEvent;
use App\Service\PaymentFlow\VendorResult;
use Illuminate\Support\Facades\DB;

class PaymentService
{
    protected $apiClient;

    protected $paymentApiMethod;

    protected $paymentApiUrl;

    public function __construct()
    {
        $this->apiClient = new \GuzzleHttp\Client();
        $this->paymentApiUrl = env("PAYMENT_API_URL");
        $this->paymentApiMethod = env("PAYMENT_API_METHOD");
    }

    public function check()
    {
        $payments = $this->findPayments();
        foreach ($payments as $payment) {
            $result = $this->triggerAPI($payment);
            if (!$result->isAPISuccess()) {
                $this->handelVendorFail($result);
                continue;
            }
            $this->handleVendorSuccess($result);
        }
    }

    protected function triggerAPI($payment)
    {
        try {
            $result = $this->apiClient->request(
                $this->paymentApiMethod,
                $this->paymentApiUrl, [
                    'id' => $payment->id
                ]);
            return new VendorResult($payment, $result);
        } catch (\Exception $exception) {
            return new VendorResult($payment, $exception);
        }
    }

    protected function findPayments()
    {
        $today = date('Y-m-d');
        return DB::table('payments')
            ->orderBy('id')
            ->where('is_paid', '=', false)
            ->where('expired_at', ">=", $today)
            ->get();
    }

    protected function handleVendorSuccess(VendorResult $result)
    {
        //todo
        event(new SendPaymentSuccessEmailEvent($result));
    }

    protected function handelVendorFail(VendorResult $result)
    {
        //todo
    }
}
