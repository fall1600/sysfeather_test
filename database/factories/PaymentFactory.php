<?php

use Faker\Generator as Faker;

$factory->define(App\Payment::class, function (Faker $faker) {
    return [
        'transaction_id' => $faker->sha256,
        'amount' => $faker->numberBetween(100, 20000),
        'is_paid' => $faker->boolean,
        'email' => $faker->email,
        'expired_at' => $faker->dateTimeBetween("-20 days", '+3 days'),
    ];
});
